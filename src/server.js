/* Load 'socket.io' module */
const port = 3000;
const io = require('socket.io')(port);

/* Load 'elasticsearch' module */
const elasticsearch = require('@elastic/elasticsearch');
const es_client = new elasticsearch.Client({ node: 'http://localhost:9200' });

/* Load actions of packet */
const join = require('./server/actions/join');
const broadcast = require('./server/actions/broadcast');
const list = require('./server/actions/list');
const quit = require('./server/actions/quit');
const send = require('./server/actions/send');
const joinGroup = require('./server/actions/join-group');
const groupBroadcast = require('./server/actions/group-broadcast');
const members = require('./server/actions/members');
const messagesHistory = require('./server/actions/messages-history');
const userMessagesHistory = require('./server/actions/user-messages-history');
const groupList = require('./server/actions/group-list');
const leave = require('./server/actions/leave');
const secure1 = require('./server/actions/secure-1');
const secure2 = require('./server/actions/secure-2');
const secureSend = require('./server/actions/secure-send');
const kick = require('./server/actions/kick');
const invite = require('./server/actions/invite');

/* When start a server */
console.log("Server is listening on port: %d", port);

/* Listen to the 'connect' event that fired upon a successfull connection */
io.on('connect', (socket) => {
    console.log("A user connected");

    /* Listen to 'disconnect' event that fired upon a successfull disconnection */
    socket.on('disconnect', (reason) => {
        console.log("A user disconnected, reason: %s", reason);
        console.log("Number of users: %d", io.of('/').server.engine.clientsCount);
    });

    /* Listen to 'packet' event from an individual socket */
    socket.on('packet', (packet_data) => {
        switch (packet_data.action) {
            case "join":
                join(io, socket, packet_data, es_client);
                break;

            case "broadcast":
                broadcast(socket, packet_data, es_client);
                break;

            case "list":
                list(io, socket, packet_data);
                break;

            case "quit":
                quit(socket, packet_data);
                break;

            case "send":
                send(io, socket, packet_data, es_client);
                break;

            /* Create a group: cg;friends */
            case "cgroup":
                console.log("Will suggest use j:groupname");
                break;

            /* Join a group: j;friends */
            case "jgroup":
                joinGroup(socket, packet_data);
                break;

            /* Broadcast a message to a group: bg;friends;hello */
            case "gbroadcast":
                groupBroadcast(socket, packet_data, es_client);
                break;

            /* List all clients that are inside a group: members;friends */
            case "members":
                members(io, socket, packet_data);
                break;

            /* List the history of messages exchanged in a group:
            messages;friends */
            case "msgs":
                messagesHistory(socket, packet_data, es_client);
                break;

            /* List the history of messages belonging to a user:
            umessages;ted */
            case "umsgs":
                userMessagesHistory(socket, packet_data, es_client);
                break;

            /* List the existing groups: groups; */
            case "groups":
                groupList(io, socket, packet_data);
                break;

            /* Leave a group: leave;friends */
            case "leave":
                leave(io, socket, packet_data);
                break;

            /* Kick a member out of group: kick;groupname;member;reason-message */
            case "kick":
                kick(io, packet_data);
                break;

            /* Invite a member to a group: invite;groupname;username */
            case "invite":
                invite(io, packet_data);
                break;

            case "secure_1":
                secure1(io, packet_data);
                break;

            case "secure_2":
                secure2(io, packet_data);
                break;

            case "secure_send":
                secureSend(io, packet_data);
                break;

            /* For debugging: kkk; */
            case "kkk":
                console.log(io.of('/'));
                break;

            default:
                break;
        }

    });
});