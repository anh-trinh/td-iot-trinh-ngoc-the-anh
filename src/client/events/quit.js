module.exports = (socket, nickname) => {
    socket.emit('packet', {"sender": nickname, "action": "quit"});
}