module.exports = (socket, input, nickname) => {
    const str = input.slice(9);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "msgs",
            "group": str
        });
}