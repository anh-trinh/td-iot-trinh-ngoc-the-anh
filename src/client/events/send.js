module.exports = (socket, input, pattern, nickname) => {
    const info = input.match(pattern);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "send",
            "dest": info[1],
            "msg": info[2]
        });
}