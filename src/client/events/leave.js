module.exports = (socket, input, nickname) => {
    const str = input.slice(6);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "leave",
            "group": str
        });
}