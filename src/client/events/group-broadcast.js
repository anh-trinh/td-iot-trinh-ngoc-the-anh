module.exports = (socket, input, pattern_2, nickname) => {
    const info = input.match(pattern_2);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "gbroadcast",
            "group": info[1],
            "msg": info[2]
        });
}