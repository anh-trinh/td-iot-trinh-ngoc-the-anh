module.exports = (socket, input, nickname) => {
    const str = input.slice(8);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "members",
            "group": str
        });
}