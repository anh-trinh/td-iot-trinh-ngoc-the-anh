module.exports = (socket, input, crypto, nickname) => {
    const str = input.slice(3);

    // Create an ecdh instance with key size of 256 bits
    const ecdh = crypto.createECDH('secp256k1');

    // Generates private and public key, and returns the public key
    const my_public_key = ecdh.generateKeys();

    socket.emit('packet',
        {
            "sender": nickname,
            "action": "secure_1",
            "dest": str,
            "public_key": my_public_key
        });
}