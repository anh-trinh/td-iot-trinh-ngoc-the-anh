module.exports = (socket, input, nickname) => {
    const str = input.slice(2);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "jgroup",
            "group": str
        });
}