module.exports = (socket, input, nickname, pattern_5) => {
    const info = input.match(pattern_5);
    socket.emit("packet",
        {
            "sender": nickname,
            "action": "kick",
            "group": info[1],
            "dest": info[2],
            "reason": info[3]
        });
}