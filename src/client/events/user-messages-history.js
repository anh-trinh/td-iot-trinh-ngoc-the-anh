module.exports = (socket, input, nickname) => {
    const str = input.slice(10);
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "umsgs",
            "user": str
        });
}