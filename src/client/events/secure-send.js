module.exports = (socket, input, crypto, nickname, secret_key, pattern_3) => {
    const info = input.match(pattern_3);

    // Length of secret key is dependent on the algorithm. In this case for aes256,
    // it requires 32 bytes (256 bits), please pay attention that our secret key
    // generated in the establish steps meet this requirement
    const algorithm = 'aes-256-cbc';

    // Initialization vector
    // Block sizes of AES are 128 bits, iv has the same size as the block size
    const randome_iv = crypto.randomBytes(16);

    // Create cipher object with the given arguments
    const cipher = crypto.createCipheriv(algorithm, secret_key, randome_iv);

    // Start to encrypt
    let encrypted = cipher.update(info[2] /* raw message */, 'utf8', 'hex');
    encrypted += cipher.final('hex');

    // Send iv along with the encrypted message for decrypting message
    socket.emit('packet',
        {
            "sender": nickname,
            "action": "secure_send",
            "dest": info[1],
            "encrypted_msg": encrypted,
            "iv": randome_iv
        });
}