module.exports = (socket, input, nickname, pattern_4) => {
    const info = input.match(pattern_4);
    socket.emit("packet",
        {
            "sender": nickname,
            "action": "invite",
            "group": info[1],
            "dest": info[2]
        });
}