module.exports = (socket, packet_data, crypto, nickname) => {
    const partner_public_key = packet_data.public_key;

    // Create an ecdh instance with key size of 256 bits
    const ecdh = crypto.createECDH('secp256k1');

    // Generates private and public key, and returns the public key
    const my_public_key = ecdh.generateKeys();

    // Send my public key back to the sender
    socket.emit('packet', {"sender": nickname, "action": "secure_2",
        "dest": packet_data.sender, "public_key": my_public_key});
    // Generates the secret key, please pay attention that this key is the same at two ends
    const secret_key = ecdh.computeSecret(partner_public_key);

    // For debugging
    // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
}