module.exports = (packet_data) => {
    console.log(`[INFO] User ${packet_data.dest} has been removed out of the group ${packet_data.group}, reason: ${packet_data.reason}`);
}