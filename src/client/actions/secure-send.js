module.exports = (packet_data, crypto) => {
    // Use the same algorithm as the sender
    const algorithm = 'aes-256-cbc';

    // Initialization vector from the sender
    const iv = packet_data.iv;

    // Create decipher object with the given arguments
    const decipher = crypto.createDecipheriv(algorithm, secret_key, iv);

    // Start to decrypt the message from the sender
    let decrypted = decipher.update(packet_data.encrypted_msg, 'hex', 'utf8');
    decrypted += decipher.final('utf8');

    // Show the decrypted message at our end
    console.log("%s", decrypted);
}