module.exports = (packet_data, crypto) => {
    const partner_public_key = packet_data.public_key;

    // Create an ecdh instance with key size of 256 bits
    const ecdh = crypto.createECDH('secp256k1');

    // Generates the secret key, please pay attention that this key is the same at two ends
    secret_key = ecdh.computeSecret(partner_public_key);

    // For debugging
    // console.log("[INFO]: Secret key: %s", secret_key.toString('hex'));
}