/*
    The custom event name: packet

    Request in JSON format:
    { sender: who, action: "join", password: what }
    { sender: who, action: "broadcast", msg: what }
    { sender: who, action: "list" }
    { sender: who, action: "quit" }

    { sender: who, action: "send", dest: whom, msg: what }
    { sender: who, action: "cgroup", group: name }
    { sender: who, action: "jgroup", group: name }
    { sender: who, action: "gbroadcast", group: name, msg: what }
    { sender: who, action: "members", group: name }
    { sender: who, action: "msgs", group: name }
    { sender: who, action: "umsgs", user: name }
    { sender: who, action: "groups"}
    { sender: who, action: "leave", group: name }

    The first step of establishing secure connection
    { sender: who, action: "secure_1", dest: whom, public_key: what }
    The second step
    { sender: who, action: "secure_2", dest: whom, public_key: what }

    After that, we can send an encrypted message along with iv
    { sender: who, action: "secure_send", dest: whom, encrypted_msg: what, iv: what }
*/

const io = require('socket.io-client'); /* Load 'socket.io-client' module */
const socket = io('http://localhost:3000'); /* Connect to the server */
const readline = require('readline'); /* Load built-in 'readline' module */
const crypto = require('crypto'); /* Load built-in 'crypto' module */
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let nickname = null;
let password = null;

/* Take a look at https://regexr.com/ for testing */
const pattern = /^s;([A-Z0-9]+);(.+)/i; // for sending privately to someone
const pattern_2 = /^bg;([A-Z0-9]+);(.+)/i; // for group broadcast
const pattern_3 = /^sss;([A-Z0-9]+);(.+)/i; // for sending an encrypted message
const pattern_4 = /^invite;([A-Z0-9]+);(.+)/i; // to invite an user into the group
const pattern_5 = /^kick;([A-Z0-9]+);([A-Z0-9]+);(.+)/i; // to kick an user into the group

/* Load actions of packet */
const join = require('./client/actions/join');
const broadcast = require('./client/actions/broadcast');
const list = require('./client/actions/list');
const quit = require('./client/actions/quit');
const send = require('./client/actions/send');
const joinGroup = require('./client/actions/join-group');
const groupBroadcast = require('./client/actions/group-broadcast');
const members = require('./client/actions/members');
const messagesHistory = require('./client/actions/messages-history');
const userMessagesHistory = require('./client/actions/user-messages-history');
const groupList = require('./client/actions/group-list');
const leave = require('./client/actions/leave');
const secure1 = require('./client/actions/secure-1');
const secure2 = require('./client/actions/secure-2');
const secureSend = require('./client/actions/secure-send');
const kick = require('./client/actions/kick');
const invite = require('./client/actions/invite');

/* Load events */
const broadcastEvents = require('./client/events/broadcast');
const listEvents = require('./client/events/list');
const quitEvents = require('./client/events/quit');
const sendEvents = require('./client/events/send');
const joinGroupEvents = require('./client/events/join-group');
const groupBroadcastEvents = require('./client/events/group-broadcast');
const membersEvents = require('./client/events/members');
const messagesHistoryEvents = require('./client/events/messages-history');
const userMessagesHistoryEvents = require('./client/events/user-messages-history');
const groupListEvents = require('./client/events/group-list');
const leaveEvents = require('./client/events/leave');
const secureEvents = require('./client/events/secure');
const secureSendEvents = require('./client/events/secure-send');
const kickEvents = require('./client/events/kick');
const inviteEvents = require('./client/events/invite');

/* For secure connection */
var secret_key;

console.log("Connecting to the server...");

/* Listen to the 'connect' event that fired upon a successfull connection */
socket.on('connect', () => {
    /* All command-line arguments received by the shell are given to the
    process in an array called argv (short for 'argument values')
    Node.js exposes this array for every running process in the form of
    process.argv
    Syntax on command line: node client <nickname> <password> */
    nickname = process.argv[2];
    password = process.argv[3];
    console.log("[INFO]: Welcome %s", nickname);
    socket.emit('packet', {"sender": nickname, "action": "join", "password": password});
});

/* Listen to the 'disconnect' event that fired upon a successfull disconnection */
socket.on('disconnect', (reason) => {
    console.log("[INFO]: Server disconnected, reason: %s", reason);

    /* Close the rl instance. When called, the 'close' event will be emitted */
    rl.close();
});

/* The 'line' event is emitted whenever the input stream receives
an end-of-line input (\n, \r, or \r\n). This usually occurs when
the user presses the <Enter>, or <Return> keys */
rl.on('line', (input) => {
    /* The handler function is called with a string containing
    the single line of received input */

    /* Broadcast a message: b;hello */
    if (true === input.startsWith("b;")) {
        broadcastEvents(socket, input, nickname);
    }

    /* List all nicknames in the chat: ls; */
    else if ("ls;" === input) {
        listEvents(socket, nickname);
    }

    /* Quit the chat: q; */
    else if ("q;" === input) {
        quitEvents(socket, nickname);
    }

    /* Send a message secretly to someone: s;ted;hello ted */
    else if (true === pattern.test(input)) {
        sendEvents(socket, input, pattern, nickname);
    }

    /* Create a group: cg;friends */
    else if (true === input.startsWith("cg;")) {
        console.log("j:groupname will also handle join with this if group not exist");
    }

    /* Join a group: j;friends */
    else if (true === input.startsWith("j;")) {
        joinGroupEvents(socket, input, nickname);
    }

    /* Broadcast a message to a group: bg;friends;hello */
    else if (true === pattern_2.test(input)) {
        groupBroadcastEvents(socket, input, pattern_2, nickname);
    }

    /* List all clients that are inside a group: members;friends */
    else if (true === input.startsWith("members;")) {
        membersEvents(socket, input, nickname);
    }

    /* List the history of messages exchanged in a group:
    messages;friends */
    else if (true === input.startsWith("messages;")) {
        messagesHistoryEvents(socket, input, nickname);
    }

    /* List the history of messages belonging to a user:
    umessages;ted */
    else if (true === input.startsWith("umessages;")) {
        userMessagesHistoryEvents(socket, input, nickname);
    }

    /* List the existing groups: groups; */
    else if ("groups;" === input) {
        groupListEvents(socket, nickname);
    }

    /* Leave a group: leave;friends */
    else if (true === input.startsWith("leave;")) {
        leaveEvents(socket, input, nickname);
    }

        /* Establish a secure connection with Ted: ss;ted */
    // Share key: https://asecuritysite.com/encryption/js_ecdh
    else if (true === input.startsWith("ss;")) {
        secureEvents(socket, input, crypto, nickname);
    }

    /* Send an encrypted message to Ted: sss;ted;hello ted */
    else if (true === pattern_3.test(input)) {
        secureSendEvents(socket, input, crypto, nickname, secret_key);
    }

    /* Invite user Ted into specified group IoT: invite;IoT;Ted */
    else if (pattern_4.test(input) === true) {
        inviteEvents(socket, input, nickname, pattern_4);
    }

    /* Kick user Ted from specified group Comic because he want to focus on studying:
    kick;Comic;Ted;focus on studying */
    else if (pattern_5.test(input) === true) {
        kickEvents(socket, input, nickname, pattern_5);
    }

    /* For debugging: kkk; */
    else if ("kkk;" === input) {
        socket.emit('packet', {"sender": nickname, "action": "kkk"});
    }

    else {
        console.log("[ERROR]: don't support this kind of syntax");
    }
});

/* Listen to the 'packet' event that is emitted from the server */
socket.on('packet', (packet_data) => {
    switch (packet_data.action) {
        case "join":
            join(packet_data);
            break;

        case "broadcast":
            broadcast(packet_data);
            break;

        case "list":
            list(packet_data);
            break;

        case "quit":
            quit(packet_data);
            break;

        case "send":
            send(packet_data);
            break;

        case "cgroup":
            console.log("Please use j:groupname for automatically create when group not exist");
            break;

        case "jgroup":
            joinGroup(packet_data);
            break;

        case "gbroadcast":
            groupBroadcast(packet_data);
            break;

        case "members":
            members(packet_data);
            break;

        case "msgs":
            messagesHistory(packet_data);
            break;

        case "umsgs":
            userMessagesHistory(packet_data);
            break;

        case "groups":
            groupList(packet_data);
            break;

        case "leave":
            leave(packet_data);
            break;

        case "secure_1":
            secure1(socket, packet_data, crypto, nickname);
            break;

        case "secure_2":
            secure2(packet_data, crypto);
            break;

        case "secure_send":
            secureSend(packet_data, crypto);
            break;

        case "invite":
            invite(packet_data);
            break;

        case "kick":
            kick(packet_data);
            break;

        default:
            break;
    }
});