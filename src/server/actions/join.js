/* Load bcrypt module */
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = (io, socket, packet_data, es_client) => {
    console.log("Nickname: ", packet_data.sender, ", ID: ", socket.id);
    console.log("Number of users: %d", io.of('/').server.engine.clientsCount);

    /* Check if a nickname exists */
    /*  index: accounts
        documents in the index:
            { "nickname": "ted", "password": "hi" }
            { "nickname": "roni", "password": "hi" }
    */
    es_client.search({
        index: "accounts",

        /* Search queries */
        body: {
            "query": {
                "match": {
                    "nickname": packet_data.sender
                }
            }
        }
    }, (err, result) => {
        if (err) console.log(err);
        if (404 === result.statusCode ||
            /* Index not found, this means this is the first time that a user join,
                so just save the account information */

            (200 === result.statusCode && null === result.body.hits.max_score)) {
            /* Nickname not found, this means this is the first time that a user join,
                so just save the account information */

            if (404 === result.statusCode) { console.log("Index not found!"); }
            else { console.log("Nickname not found!"); }

            bcrypt.hash(packet_data.password, saltRounds, (err, hashed_password) => {
                /* Adds a JSON document to the specified index and makes it searchable.
                If the document already exists, updates the document and increments its version.*/
                es_client.create({
                    /* Document ID */
                    id: new Date().getTime(),

                    /* By default, the index is created automatically if it doesn’t exist */
                    index: "accounts",

                    /* If true then refresh the affected shards to make this operation visible
                    to search */
                    refresh: 'true',

                    /* Document data */
                    body: {
                        "nickname": packet_data.sender,
                        "password": hashed_password
                    }
                }, (err, result) => {
                    if (err) console.log(err);
                    if (201 === result.statusCode) {
                        console.log("Saved the account to the database");
                    }
                });
            });

        } else {
            /* Found the nickname, check if the password matches or not */
            bcrypt.compare(packet_data.password, result.body && result.body.hits.hits[0]._source.password,
                (err, result) => {
                    if (true === result) {
                        console.log("Correct password");

                        /*  Broadcasting means sending a packet to everyone else
                        except for the socket that starts it */
                        socket.broadcast.emit('packet', packet_data);
                    } else {
                        console.log("Incorrect password!");
                        socket.disconnect(true);
                    }
                });
        }
    });

    /* Save nickname to this socket object */
    socket.nickname = packet_data.sender;
}