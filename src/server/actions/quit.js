module.exports = (socket, packet_data) => {
    /*  Broadcasting means sending a packet to everyone else
                    except for the socket that starts it */
    socket.broadcast.emit('packet', packet_data);
    socket.disconnect(true);
}