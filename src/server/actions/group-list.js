module.exports = (io, socket, packet_data) => {
    const rooms = io.of('/').adapter.rooms;

    /* Sending a packet to socket that starts it */
    socket.emit('packet', {"sender": packet_data.sender,
        "action": "groups",
        "groups": Object.keys(rooms)});
}