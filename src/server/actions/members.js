module.exports = (io, socket, packet_data) => {
    let members = [];

    io.of('/').in(packet_data.group).clients((error, clients) => {
        if (error) throw error;

        /* clients are an array of socket ids in this group */
        for (var i = clients.length - 1; i >= 0; i--) {
            members.push(io.of('/').sockets[clients[i]].nickname);
        }

        /* Sending a packet to socket that starts it */
        socket.emit('packet', {"sender": packet_data.sender,
            "action": "members",
            "group": packet_data.group,
            "members": members});
    });
}