module.exports = (socket, packet_data, es_client) => {
    /*  Broadcasting means sending a packet to everyone else
                except for the socket that starts it */
    socket.broadcast.emit('packet', packet_data);

    /* Save all messages to elasticsearch
        index: room_messages
        documents in the index:
            { "group": socket.id, "sender": ted, "msg": "hi" }
            { "group": socket.id, "sender": roni, "msg": "hi" }
    */
    /* Adds a JSON document to the specified index and makes it searchable.
    If the document already exists, updates the document and increments its version.*/
    es_client.create({
        /* Document ID */
        id: new Date().getTime(),

        /* By default, the index is created automatically if it doesn’t exist */
        index: "room_messages",

        /* If true then refresh the affected shards to make this operation visible
        to search */
        refresh: 'true',

        /* Document data */
        body: {
            "group": socket.id,
            "sender": packet_data.sender,
            "msg": packet_data.msg
        }
    }, (err, result) => {
        if (err) console.log(err);
        if (201 === result.statusCode) {
            console.log("Saved the message to the database");
        }
    });
}