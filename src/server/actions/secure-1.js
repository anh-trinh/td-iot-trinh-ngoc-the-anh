module.exports = (io, packet_data) => {
    let socket_id = null;

    console.log("[%s]: Public key: %s", packet_data.sender,
        packet_data.public_key.toString('hex'));

    /* Search all nicknames */
    for (let key in io.of('/').sockets) {
        if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
            socket_id = io.of('/').sockets[key].id;
        }
    }

    if (socket_id !== null) {
        /* Each socket is identified by a random,
        unguessable, unique identifier Socket#id. For your convenience,
        each socket automatically joins a room identified by its own id. */
        io.to(socket_id).emit('packet', packet_data);
    }
}