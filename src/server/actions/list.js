module.exports = (io, socket, packet_data) => {
    let users = [];

    /* Search all nicknames */
    for (let key in io.of('/').sockets) {
        users.push(io.of('/').sockets[key].nickname);
    }

    /* Sending a packet to socket that starts it */
    socket.emit('packet', {"sender": packet_data.sender,
        "action": "list",
        "users": users});
}