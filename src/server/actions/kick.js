module.exports = (io, packet_data) => {
    console.log("[INFO] Kick user <%s> out of group <%s>");
    let socket_id = null;
    for(var key in io.of("/").sockets){
        if (packet_data.dest.toLowerCase() === io.of("/").sockets[key].nickname.toLowerCase()) {
            socket_id = io.of("/").sockets[key].id;
        }
    }

    if (socket_id != null) {
        // console.log(socket_id);
        io.sockets.connected[socket_id].leave(packet_data.group, () => {
            console.log("[INFO] User <", packet_data.sender, "> left the group <", packet_data.group, ">");
            io.to(packet_data.group).emit('packet', packet_data);
        });
    }
}