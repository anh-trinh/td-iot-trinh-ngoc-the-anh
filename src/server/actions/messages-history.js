module.exports = (socket, packet_data, es_client) => {
    let msgs = [];
    /*  index: room_messages
        documents in the index:
            { "group": friends, "sender": ted, "msg": "hi" }
            { "group": friends, "sender": roni, "msg": "hi" }
    */
    es_client.search({
        index: "room_messages",

        /* Search queries */
        body: {
            "query": {
                "match": {
                    "group": packet_data.group
                }
            }
        }
    }, (err, result) => {
        if (err) console.log(err);
        console.log(result);
        for (var i = 0; i < result.body.hits.hits.length; i++) {
            msgs.push(result.body.hits.hits[i]._source.sender + ": " +
                result.body.hits.hits[i]._source.msg);
        }

        /* Sending a packet to socket that starts it */
        socket.emit('packet', {"sender": packet_data.sender,
            "action": "msgs",
            "group": packet_data.group,
            "msgs": msgs});
    });
}