module.exports = (socket, packet_data) => {
    socket.join(packet_data.group, () => {
        console.log("Group: ", packet_data.group, ", Joined: ", packet_data.sender);

        /* Sending to all clients in a group, including sender */
        // io.to(packet_data.group).emit('packet', packet_data);

        /* Sending to all clients in a group except sender */
        socket.to(packet_data.group).emit('packet', packet_data);
    });
}