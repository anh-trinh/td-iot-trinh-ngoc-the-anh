module.exports = (socket, packet_data, es_client) => {
    let msgs = [];
    /*  index: room_messages
        documents in the index:
            { "group": friends, "sender": ted, "msg": "hi" }
            { "group": friends, "sender": roni, "msg": "hi" }
    */
    es_client.search({
        index: "room_messages",

        /* Search queries */
        body: {
            "query": {
                "match": {
                    "sender": packet_data.user
                }
            }
        }
    }, (err, result) => {
        if (err) console.log(err);

        for (var i = 0; i < result.body.hits.hits.length; i++) {
            msgs.push(result.body.hits.hits[i]._source.group + ": " +
                result.body.hits.hits[i]._source.msg);
        }

        /* Sending a packet to socket that starts it */
        socket.emit('packet', {"sender": packet_data.sender,
            "action": "umsgs",
            "user": packet_data.user,
            "msgs": msgs});
    });
}