module.exports = (io, packet_data) => {
    console.log(`[INFO] Invite user ${packet_data.dest} to group `)
    var socket_id = null;
    for(var key in io.of("/").sockets){
        if (packet_data.dest.toLowerCase() === io.of("/").sockets[key].nickname.toLowerCase()) {
            socket_id = io.of("/").sockets[key].id;
        }
    }

    if (socket_id != null) {
        // console.log(socket_id);
        io.sockets.connected[socket_id].join(packet_data.group);
        io.to(packet_data.group).emit("packet", packet_data);
    }
}