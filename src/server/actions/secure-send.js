module.exports = (io, packet_data) => {
    let socket_id = null;

    console.log("[%s]: Encrypted message: %s\n\tIV: %s", packet_data.sender,
        packet_data.encrypted_msg,
        packet_data.iv.toString('hex'));

    /* Search all nicknames */
    for (var key in io.of('/').sockets) {
        if (packet_data.dest.toLowerCase() === io.of('/').sockets[key].nickname) {
            socket_id = io.of('/').sockets[key].id;
        }
    }

    if (socket_id !== null) {
        /* Each socket is identified by a random,
        unguessable, unique identifier Socket#id. For your convenience,
        each socket automatically joins a room identified by its own id. */
        io.to(socket_id).emit('packet', packet_data);
    }
}