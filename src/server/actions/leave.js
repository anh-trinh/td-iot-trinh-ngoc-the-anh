module.exports = (io, socket, packet_data) => {
    socket.leave(packet_data.group, () => {
        console.log("Group: ", packet_data.group, ", Left: ", packet_data.sender);

        /* Sending to all clients in a group, including sender */
        io.to(packet_data.group).emit('packet', packet_data);
    });
}